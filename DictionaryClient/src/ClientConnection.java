import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class ClientConnection implements ClientConnectionConstants{

	
	public ClientConnection() {
		initJFrame();
        
		System.out.println("Try to get connection to server.");
		frame.appendTextToStatus("Try to get connection to server.");
		String host = "127.0.0.1";
		int portNumber = 8888 ;
		try {
			Socket s = new Socket(host, portNumber);	
			in = new Scanner(s.getInputStream());
			out = new PrintWriter(s.getOutputStream());
			
			command = "signal";
			out.println(command);
			out.flush();
			
			while(in.hasNextLine()) {
				String input = in.nextLine();
				System.out.println(input); // output to standard output stream
                frame.appendTextToStatus(input);
			}
			
			command = "quit";
			out.println(command);
			s.close();
			
			
		} catch( IOException e) {
			frame.appendTextToStatus("Wrong Host IP address.");
			
		}
	}

    private void initJFrame() {
         frame = new ClientJFrameBean();
         frame.setVisible(true);
      
    }
    
    public void quit() {
    	if(out == null) {
    		System.exit(0);
    	}
        command = "quit";
        out.println(command);
        out.flush();
    }
    
    public void lookup() {
		Lookup l = new Lookup();
		Thread t = new Thread(l);
		t.start();
    	
    }
    
    class Lookup implements Runnable{
    	public void run() {
    		command = "lookup";
    		out.println(command);
    		out.flush();
    		
    		while(in.hasNextLine()) {
    			String input = in.nextLine();
    			System.out.println(input); // output to standard output stream
                frame.appendTextToStatus(input);
    		}
    	}
    	
    }
        
    
    
    private static ClientJFrameBean frame;
    private static Scanner in;
    private static PrintWriter out;
    private static String command; 
    
}