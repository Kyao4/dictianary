import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class ServerConnectionService implements Runnable, DictionaryConstants{
	public ServerConnectionService(Socket aSocket) {
		s = aSocket;
	}
	
	
	
	private Socket s;
	private Scanner in;
	private PrintWriter out;
	
	
	@Override
	public void run() {
		try {
			try {
				in = new Scanner(s.getInputStream());
				out = new PrintWriter(s.getOutputStream());
				doService();
			}
			finally {
				s.close();
			}
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		
	}


	private void doService() {
        while(true) {
        	if(!in.hasNextLine()) return;
		String command = in.nextLine();
		
		
		if(command.equals("signal")) {
			ServerConnection.counterPlusPlus();
			System.out.println("Current connection: " + ServerConnection.getCounterNum());
                        frame.appendTextToStatus("Current connection: " + ServerConnection.getCounterNum());
                        out.println("Connected");
                        out.flush();
		}
		if(command.equals("lookup")) {
			int size = database.getWordListSize();
			for(int i = 0; i < size; i++) {
				DictionaryWord currentWord = database.findWordInOrder(i);
				out.println(currentWord.toString());
			}
		}
		
		
		if(command.equals("quit")) {
			ServerConnection.counterMinusMinus();
                        System.out.println("Client disconnected");
                        frame.appendTextToStatus("Client disconnected");
			System.out.println("Current connection: " + ServerConnection.getCounterNum());
                        frame.appendTextToStatus("Current connection: " + ServerConnection.getCounterNum());
                        return;
		}
            } 
		
	}
	
	
        private ServerJFrameBean frame = ServerConnection.getFrame();
	
}
