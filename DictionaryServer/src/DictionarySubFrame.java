

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DictionarySubFrame extends JDialog implements DictionaryConstants, ActionListener {
	
	public DictionarySubFrame() {
		setSize(APPLICATION_WIDTH,APPLICATION_HEIGHT);
		setTitle("Lookup");
		setVisible(true);
		//setModalityType(ModalityType.APPLICATION_MODAL);
		setModal(true);
		setup();
		addListeners();
	}

	/** add listeners to components*/
	private void addListeners() {
		lookupTextField.addActionListener(this);
		lookupButton.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Lookup")) {
			String wordName = lookupTextField.getText();
			if(database.findWord(wordName).equals("-1")) {
				JOptionPane.showMessageDialog(null, "Please select your files and click compile first",
						"Compiling Warning", JOptionPane.WARNING_MESSAGE);
			} else if(database.findWord(wordName).equals("0")) {
				JOptionPane.showMessageDialog(null, "There is no " + wordName + " in this dictionary.",
						"Information", JOptionPane.INFORMATION_MESSAGE);
			} else {
				textPane.setText("");
				textPane.setText(database.findWord(wordName));
			}
		}
	}

	/** setup components and layout*/
	private void setup() {
		
		setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
		box = Box.createHorizontalBox();

		// textfield
		lookupTextField = new JTextField(TEXT_FIELD_SIZE);
		lookupTextField.setActionCommand("Lookup");
		lookupButton = new JButton("Lookup");
		p.add(lookupTextField);
		p.add(lookupButton);
		box.add(p);
		
		add(box);
		
		p = new JPanel();
		textPane.setPreferredSize(new Dimension(200,200));
		p.add(textPane);
		add(p);
		
		pack();
	}
	
	/** containers*/
	private Box box;
	private JPanel p = new JPanel();
	
	/** components*/
	private JTextField lookupTextField;
	private JTextPane textPane = new JTextPane();;
	private JButton lookupButton;
}
