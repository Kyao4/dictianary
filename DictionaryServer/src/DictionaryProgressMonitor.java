

public class DictionaryProgressMonitor implements DictionaryConstants{
	
	public DictionaryProgressMonitor() {
		counter = 0;
	}

	public void setValue(int value) {
		counter = value;
	}
	
	public void addValue() {
		counter++;
	}
	
	public int getValue() {
		return counter;
	}
	
	public void clear() {
		counter = 0;
	}
	private int counter;
}
