

import java.util.*;

public class DictionaryWord {
	
	public DictionaryWord() {
		word = null;
	}
	
	/** create key to identify each word.*/
	public DictionaryWord(String wordPassedIn) {
		word = wordPassedIn;
	}
	
	/** set the key*/
	public void setWord(String wordPassedIn) {
		word = wordPassedIn;
	}
	
	/** pass in the word line*/
	public void setWordLine(String wordLinePassedIn) {
		wordLine = wordLinePassedIn;
	}
	
	/** add every sentences which contain this word.*/
	public void addSentence(String sentenceLinePassedIn) {
		sentenceLine.add(sentenceLinePassedIn);
	}
	
	/** set one sentence to other string*/
	public void setSentence(String sentenceLinePassedIn,int num) {
		sentenceLine.set(num, sentenceLinePassedIn);
	}
	
	/** pass out the word line*/
	public String getWordLine() {
		return wordLine;
	}
	
	/** return iterator which contain the list of sentences this word contain.*/
	public Iterator<String> getSentenceIterator() {
		return sentenceLine.iterator();
	}
	
	/** return the sentence you specify.*/
	public String getSentence(int num) {
		return sentenceLine.get(num);
	}
	
	/** return the size of sentence list.*/
	public int getSentenceListSize() {
		return sentenceLine.size();
	}
	
	/** return the word this object represent.*/
	public String getWord() {
		return word;
	}
	
	/** out put the word line and its sentences in format.*/
	public String toString() {
		String wordAndSentences = wordLine + "\n\n";
		for(int i = 0; i < sentenceLine.size(); i++) {
			wordAndSentences += sentenceLine.get(i) + "\n";
		}
		wordAndSentences += "\n";
		return wordAndSentences;
	}
	private String word;
	private String wordLine;
	private ArrayList<String> sentenceLine = new ArrayList<String>();
}
