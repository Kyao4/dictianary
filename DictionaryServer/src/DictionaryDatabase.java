

import java.io.*;
import java.util.*;
import java.util.regex.*;



public class DictionaryDatabase implements DictionaryConstants{

	public DictionaryDatabase() {
		
	}
	
	public DictionaryDatabase(int i) {
		readData(); 
		constructWordList();
		constructSentenceList();
		constructDictionary();
		printOut();
    }
	

	/**compile two files into word with sentences format.
	 * @param sentenceFile this file must contain sentences
	 * @param wordFile this file must contain words in line*/
	public void compileFile(File sentenceFile, File wordFile) {
		readFiles(sentenceFile, wordFile); monitor.addValue();
		constructWordList(); monitor.addValue();
		constructSentenceList(); monitor.addValue();
		constructDictionary(); monitor.addValue();
		printOut(); monitor.addValue();
	}

	

	/** get words with sentences in string
	 * @param wordName must input a name of a word
	 * @return -1 means you didn't compile the files or didn't select a file.
	 * 0 means there is no such word you have passed in.
	 * if there is a word you passed in return the word and sentences.
	 */
	public String findWord(String wordName) {
		if(wordListMap.keySet().size() < 1) {
			return "-1";
		}
		if(wordListMap.containsKey(wordName)) {
			return wordListMap.get(wordName).toString();
		} else {
			return "0";
		}
	}
	
	/** find the word in the position you specified.*/
	public DictionaryWord findWordInOrder(int num) {
		return wordListMap.get(wordList.get(num));
	}
	
	
	/** return the size of the wordList*/
	public int getWordListSize() {
		return wordList.size();
		// TODO return wordListMap.keySet().size();
	}
	
	
	/** read in two files, the raw sentences and words into rawSentencelist 
	 * and rawWordList seperately.
	 */
	private void readFiles(File sentenceFile, File wordFile) {
		BufferedReader wordReader = createBufferedReader(wordFile);
		BufferedReader sentenceReader = createBufferedReader(sentenceFile);
		
		//temp for the reason that readline() will increase the cursor.
		String wordLine = "";
		String sentenceLine = ""; 
		try{
			while((wordLine = wordReader.readLine()) != null){
				rawWordLineList.add(wordLine.trim());
			}
			
			while((sentenceLine = sentenceReader.readLine()) != null){
				rawSentenceLineList.add(sentenceLine.trim());
			}

		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}


	/**
	 * search each sentence to match the word you are searching
	 * add the matched sentence to word in the wordListMap. 
	 * 
	 */
	private void constructDictionary() {
		for(String word : wordList){
			int sentenceCounter = 0;
			for(String sentence : sentenceList){
				String newWord = " " + word + " ";
				String newWorded = " " + word + "ed" + " ";
				
				
				if(sentence.contains(newWord) || sentence.contains(newWorded)){
						wordListMap.get(word).addSentence(SENTENCE_START + sentence);
						sentenceCounter++;
						if(sentenceCounter >= SENTENCE_QUANTITY) break;
				} 
				
			}
			
		}
		
	}


	/** 
	 * convert raw word line into word and put the word into wordList and
	 * combine the wordname and its line to DictionaryWord and add it to wordListMap.
	 */
	private void constructWordList() {
		String regex ="[a-zA-Z]*\\s";
		Pattern p = Pattern.compile(regex);
		
		for(String line : rawWordLineList){
			Matcher m = p.matcher(line);
			
			while(m.find()){
				//null count a character.
				if(m.group().length() <= 2) continue;
				word = new DictionaryWord(m.group().trim());
				word.setWordLine(WORD_START + line);
				wordListMap.put(word.getWord(),word);
				wordList.add(m.group().trim());

			}
		}		
	}


	/** convert raw sentences into sentence you are expected and put into sentenceList.*/
	private void constructSentenceList() {
		String regex ="[A-Z][a-z0-9 ,:\"-]{20,}[;]{0,}[.!?]\\s";
		Pattern p = Pattern.compile(regex);
		
		for(String line : rawSentenceLineList){
			Matcher m = p.matcher(line);
			
			while(m.find()){
				if(!m.group().contains("English")){
					sentenceList.add(m.group());	
				}
				
			}

		}		
		
	}
	
	/** read sentences and words to rawWordList and rawSentenceList. */
	private void readData() {
		BufferedReader wordReader = createBufferedReader(WORD_FILE_NAME);
		BufferedReader sentenceReader = createBufferedReader(SENTENCE_FILE_NAME);
		
		//temp for the reason that readline() will increase the cursor.
		String wordLine = "";
		String sentenceLine = ""; 
		try{
			while((wordLine = wordReader.readLine()) != null){
				rawWordLineList.add(wordLine.trim());
			}
			
			while((sentenceLine = sentenceReader.readLine()) != null){
				rawSentenceLineList.add(sentenceLine.trim());
			}

		} catch(IOException ex) {
			ex.printStackTrace();
		}
	}

	/** create a buffered reader by a file.*/
	private BufferedReader createBufferedReader(File name) {
		try {
			BufferedReader rd = new BufferedReader(new FileReader(name));
			return rd;
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		
	}  

	
	/** create a buffered reader by a name.*/
	private BufferedReader createBufferedReader(String name) {
		try {
			BufferedReader rd = new BufferedReader(new FileReader(name));
			return rd;
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		
	}  
	
	/** create a print writer by a name*/
	private PrintWriter createPrintWriter(String name) {
		try {
			PrintWriter output = new PrintWriter(new FileWriter(name));
			return output;
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		
	}  
	
	/**
	 *  print out the word and sentence.
	 * */
	private void printOut() {
		PrintWriter wt = createPrintWriter(NEW_FILE_NAME);
		for(int i = 0 ; i<wordList.size(); i++){
			String line = wordListMap.get(wordList.get(i)).toString();
			wt.println(line);
		}
		wt.close();
	}
	/*
	public static String[] decomposeWordLine(String name, String line,
			 ArrayList<String> wordList) {
		String regex = "\\s" + name + "\\s" + "|" + "\\s" + name + "ed" + "\\s";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(line);
		for(int i = 0; i < SENTENCE_QUANTITY && m.find(); i++) {
			wordList.add(m.group());
		}
		
		@SuppressWarnings("unused")
		String[] setenceList = line.split(regex);
		
		return setenceList = line.split(regex);
		
	}
	*/
	
	/** keep track of the raw wordline in the txt.*/
	private  ArrayList<String> rawWordLineList = new ArrayList<String>();
	
	/** keep track of the word clipped from the raw wordline.*/
	private  ArrayList<String> wordList = new ArrayList<String>();
	
	/** keep track of the raw sentenceline in the txt.*/
	private  ArrayList<String> rawSentenceLineList = new ArrayList<String>();
	
	/** keep track of the sentence you are expected in the raw sentenceline.*/
	private  ArrayList<String> sentenceList = new ArrayList<String>();
	
	/** combine all the information you are expected in a word.*/
	private DictionaryWord word = new DictionaryWord();
	
	/** set the word name as a key to keep track of information you need.*/
	private  Map<String, DictionaryWord> wordListMap = new HashMap<String,DictionaryWord>();
	



}
