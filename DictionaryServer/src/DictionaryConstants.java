import java.awt.Color;



public interface DictionaryConstants {
	
	/** The width of the application window */
	public static final int APPLICATION_WIDTH = 800;

	/** The height of the application window */
	public static final int APPLICATION_HEIGHT = 500;
	
	/** Number of characters for each of the text input fields */
	public static final int TEXT_FIELD_SIZE = 15;
	
	/** timer constants*/
	public static final int ONE_SECOND = 1000;
	
	/** file names and absolute path*/
 	public static final String WORD_FILE_NAME = "word.txt";
 	public static final String SENTENCE_FILE_NAME = "sentence.txt";
 	public static final String NEW_FILE_NAME = "new.txt";
 	public static final String WORKSPACE_PATH = "F:\\java\\project\\Dictionary";
 	
 	/** prefix to the sentenceline and wordline*/
 	public static final String WORD_START = "���ʣ� ";
 	public static final String SENTENCE_START = "��䣺 ";

 	public static final int SENTENCE_QUANTITY = 5;

 	
 	public static final Color WORD_COLOR = Color.RED; 	
 	public static final Color DEFAULT_WORD_COLOR = Color.BLACK; 	
 	
	
	public DictionaryDatabase database = new DictionaryDatabase();
	
	public DictionaryProgressMonitor monitor = new DictionaryProgressMonitor();
        
        public ServerConnection serverConnection = new ServerConnection();
	
	public static final int MONITOR_MAX_VALUE = 6;
	
	public static final int PROGRESSBAR_MAX_VALUE = 100;
	
	public static final int PROGRESSBAR_MIN_VALUE = 0;
	
	//public MonitorServer server = new MonitorServer();
	
}
