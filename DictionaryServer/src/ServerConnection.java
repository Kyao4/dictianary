import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;

public class ServerConnection implements DictionaryConstants, Runnable {
	@SuppressWarnings("resource")
	
	public static void main(String[] args) {
	
		mainFrame = new ServerJFrameBean();
		mainFrame.setTitle("Dictionary");
		
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
        
        public ServerConnection() {
       	
        }
        
        public void run()   {
            try{
                int portNumber = 8888;
                ServerSocket server = new ServerSocket(portNumber);
                System.out.println("Waiting for client to connect...");
                mainFrame.appendTextToStatus("Waiting for client to connect...");
        	
                while(true) {
                    Socket s = server.accept();
                    System.out.println("Client connected");
                    mainFrame.appendTextToStatus("Client connected");
                    ServerConnectionService service = new ServerConnectionService(s);
                    Thread t = new Thread(service);
                    t.start();
                }
            
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

	
	static void counterPlusPlus() {
		counter++;
	}
	
	static void counterMinusMinus() {
		counter--;
	}
	
	static int getCounterNum() {
		return counter;
	}
	
            
    public static ServerJFrameBean getFrame() {
        return mainFrame;
    }
        
	private static int counter;
    public static ServerJFrameBean mainFrame;

    
    
}