

import java.awt.event.*;
import java.awt.*;
import java.io.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.text.*;

public class DictionaryMainFrame extends JFrame implements DictionaryConstants,ActionListener, ChangeListener{

	public static void main(String[] args) {
		
		setUIManager();
		DictionaryMainFrame mainFrame = new DictionaryMainFrame();
		mainFrame.setTitle("Dictionary");
		
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public DictionaryMainFrame() {
		super();
		setUp();
		setUpListeners();
	}
	
	/** called by progressBar.setValue()*/
	@Override 
	public void stateChanged(ChangeEvent ce) {
		if(ce.getSource() == progressBar) {
			//progressBar.setForeground(Color.BLUE);
		}
	}

	/** deal with the buttons actions*/
	public void actionPerformed(ActionEvent e) {
		fileChooser.setFileFilter(new FileNameExtensionFilter("TXT","txt"));
		fileChooser.setCurrentDirectory(new File(WORKSPACE_PATH));
		if(e.getSource() == selectWordFile) {
			int returnValue = fileChooser.showOpenDialog(null);
			if(returnValue == JFileChooser.APPROVE_OPTION) {
				wordFile = fileChooser.getSelectedFile();
				wordFileText.setText(wordFile.getAbsolutePath());
			}
		}
		
		if(e.getSource() == selectSentenceFile) {
			int returnValue = fileChooser.showOpenDialog(null);
			if(returnValue == JFileChooser.APPROVE_OPTION) {
				sentenceFile = fileChooser.getSelectedFile();
				sentenceFileText.setText(sentenceFile.getAbsolutePath());
			}
		}
		
		if(e.getSource() == compile) {
			RunnableCompiler compiler = new RunnableCompiler();
			Thread t = new Thread(compiler);
			t.start();
		}
		
		/* called by timer, check out the state of the progress bar
		 * and the state of monitor, send the value of monitor to 
		 * progress bar.
		 * if progress bar got the max, stop timer
		 */
		if(e.getSource() == timer) {
			int value = progressBar.getValue();
			if(value < PROGRESSBAR_MAX_VALUE) {
				value = (int)(PROGRESSBAR_MAX_VALUE/(double)MONITOR_MAX_VALUE*monitor.getValue());
				progressBar.setValue(value);// evoke ChangeListener.
			} else {
				progressBar.setValue(PROGRESSBAR_MAX_VALUE);
				timer.stop();
			}
		}
		
		
		
		if(e.getSource() == showRedWord) {
			
			if(database.getWordListSize() > 1) {
				//timer.start();
				//monitor.clear();
				RunnableShowRedWord showRedWord = new RunnableShowRedWord();
				Thread t = new Thread(showRedWord);
				t.start();
			} else {
				JOptionPane.showMessageDialog(null, "Please select your files and " +
						"click compile button first","Compiling Warning", JOptionPane.WARNING_MESSAGE);
			}
			
			
			
		}
	}
	
	class RunnableShowRedWord implements DictionaryConstants, Runnable {
		public void run() {
			showColorfulWord(WORD_COLOR);
		}
	}

	/** put compiling into a runnable class*/
	class RunnableCompiler implements DictionaryConstants, Runnable {

		@Override
		public void run() {
			if( sentenceFile != null && wordFile != null) {
				progressBar.setValue(0);
				// start progress countering.
				timer.start();
				// start compiling
				database.compileFile(sentenceFile, wordFile);
				showUpWords(); monitor.addValue();
			} else {
				JOptionPane.showMessageDialog(null, "Please select your files first",
						"Compiling Warning", JOptionPane.WARNING_MESSAGE);
			}
		}
		
	}
	
	private void showColorfulWord(Color c) {
		textPane.setEditable(false);
		int size = database.getWordListSize();
		textPane.setText("");
		for(int i = 0; i < size; i++) {
			DictionaryWord currentWord = database.findWordInOrder(i);
			String[] decomposedLineList = decomposeWordLine(currentWord.getWord(),currentWord.toString());
			String wordName = ' ' + currentWord.getWord();
			for(int j = 0; j < decomposedLineList.length; j++) {
				insertWord(decomposedLineList[j], DEFAULT_WORD_COLOR);
				if(j == 0) {
					insertWord(wordName, DEFAULT_WORD_COLOR); continue;
				}
				if(j == decomposedLineList.length - 1) break;
				insertWord(wordName, c);
			}
		}
		
	}
	
	private String[] decomposeWordLine(String name, String line) {
		String regex = "\\s" + name;
		String[] decomposedLineList = line.split(regex);
		return decomposedLineList;
	}

	private void showUpWords() {
		textPane.setEditable(false);
		int size = database.getWordListSize();
		textPane.setText("");
		for(int i = 0; i < size; i++) {
			DictionaryWord currentWord = database.findWordInOrder(i);
			insertWord(currentWord.toString(),DEFAULT_WORD_COLOR);
			//setWordColor(currentWord, Color.red);
		}
		
	}
	
	/**
	 * 插入有颜色的字体
	 * @param str
	 * @param c
	 */
	private void insertWord(String str, Color c) {
		SimpleAttributeSet attri = new SimpleAttributeSet();
		StyleConstants.setForeground(attri, c);
		try {
			doc.insertString(doc.getLength(), str, attri);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
		
	}
	

	/** add listeners*/
	private void setUpListeners() {
		selectWordFile.addActionListener(this);
		selectSentenceFile.addActionListener(this);
		compile.addActionListener(this);
		showRedWord.addActionListener(this);
	}

	
	/**
	 * setup all the components and layout*/
	private void setUp() {

		setSize(APPLICATION_WIDTH,APPLICATION_HEIGHT);
		getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
		
		// create sub frame menu item.
		JMenuItem lookup = new JMenuItem("Lookup");
		lookup.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DictionarySubFrame subFrame = new DictionarySubFrame();
				subFrame.setVisible(true);
			}
		});
		
		
		// create exit menu item.
		JMenuItem quitItem = new JMenuItem("quit");
		quitItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		// create menu for lookup word and quit.
		JMenu function = new JMenu("Function");
		function.add(lookup);
		function.add(quitItem);
		

		
		// create menuBar and use it.
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(function);
		setJMenuBar(menuBar);
		
		
		
		// add textfield
		box = Box.createHorizontalBox();
		
		p = new JPanel();
		wordFileText.setEditable(false);
		p.add(wordFileText);
		box.add(p);

		p = new JPanel();
		sentenceFileText.setEditable(false);
		p.add(sentenceFileText);
		box.add(p);
		
		add(box);
		

		// add select file buttons
		box = Box.createHorizontalBox();
		
		p = new JPanel();
		selectWordFile = new JButton("Select Word File..");
		p.add(selectWordFile);
		box.add(p);
		
		p = new JPanel();
		selectSentenceFile = new JButton("Select Sentence File..");
		p.add(selectSentenceFile);
		box.add(p);
		
		add(box);
		
		// add compile button
		box = Box.createHorizontalBox();
		
		p = new JPanel();
		compile = new JButton("Compile..");
		p.add(compile);
		box.add(p);

		p = new JPanel();
		showRedWord = new JButton("Show Red Word..");
		p.add(showRedWord);
		box.add(p);
		
		add(box);
		
		
		// create JTextPane
		p = new JPanel();
		
		scrollPane= new JScrollPane(textPane,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(300,300));
		p.add(scrollPane);
		add(p);
		
		// add progress bar.
		progressBar.setOrientation(JProgressBar.HORIZONTAL);
		progressBar.setMinimum(PROGRESSBAR_MIN_VALUE);
		progressBar.setMaximum(PROGRESSBAR_MAX_VALUE);
		progressBar.setValue(PROGRESSBAR_MIN_VALUE);
		progressBar.setStringPainted(true);
		progressBar.addChangeListener(this);
		progressBar.setPreferredSize(new Dimension(300,20));
		progressBar.setBorderPainted(true);
		progressBar.setForeground(Color.BLUE);
		progressBar.setBackground(Color.WHITE);
		add(progressBar);
		
		// initialize timer.
		timer = new Timer(1,this);
		
		pack();


		
	}
	
	private static void setUIManager() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}
	


	/** file processing objects*/
	private JFileChooser fileChooser = new JFileChooser();
	private File sentenceFile;
	private File wordFile;
	
	/** layout and components*/
	private JTextField wordFileText = new JTextField(TEXT_FIELD_SIZE);
	private JTextField sentenceFileText = new JTextField(TEXT_FIELD_SIZE);
	private JButton selectWordFile;
	private JButton selectSentenceFile;
	private JButton compile;
	private JButton showRedWord;
	private JTextPane textPane = new JTextPane();
	private JScrollPane scrollPane;
	private JProgressBar progressBar = new JProgressBar();
	private Document doc = textPane.getDocument();
	
	/** containers*/
	private JPanel p;
	private Container box;

	/** timer associated with progress bar.*/
	Timer timer;
	
}
